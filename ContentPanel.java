package portfolio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ContentPanel extends JPanel{
	private BufferedImage image;
	
	public ContentPanel() throws IOException {
		super();
        
        image = ImageIO.read(new File("./SectionPanelTexture.png"));
        
        
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, this);
	}
	
	public BufferedImage getImage() {
		return image;
	}
}
