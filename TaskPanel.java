package portfolio;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class TaskPanel extends JPanel{
	
	private String task;
	private int ID;
	private SectionPanel parentPanel;
	private String parentPanelStr;
	private JLabel taskLabel;
	
	private BufferedImage image;
	
	public void refresh() {
		//System.out.println("TaskPanel: " + task + ", ID of: " + ID + ", ParentPanel: " + parentPanel.getTitle());
	}
	
	public TaskPanel(String t, int ID, String parentPanelStr) throws IOException {
		super();
		task = t;
		this.ID = ID;
		this.parentPanelStr = parentPanelStr;
		System.out.println("TaskPanel: " + t + ", ID of: " + ID + ", ParentPanel: " + parentPanelStr);
		
		image = ImageIO.read(new File("./TaskPanelTexture.png"));
		
		setLayout(new FlowLayout());
		setBackground(new Color(201, 201, 201));
		
		
		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.WHITE, 1), BorderFactory.createRaisedBevelBorder()));
		taskLabel = new JLabel();
		
		t = String.format("<html><div WIDTH=%d>%s</div></html>", this.getWidth(), t);
		taskLabel.setHorizontalAlignment(JLabel.CENTER);
		
		
		taskLabel.setText(t);
		
		
		taskLabel.setForeground(Color.BLACK);
		
		add(taskLabel);
		setVisible(true);
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(image, 0, 0, this);
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public String getTask() {
		return task;
	}
	
	public int getID() {
		return ID;
	}
	
	public void changeID(int newID) {
		this.ID = newID;
	}
	
	public SectionPanel getParentPanel() {
		return parentPanel;
	}

	public String getParentPanelStr() {
		return parentPanelStr;
	}
}