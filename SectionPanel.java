package portfolio;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class SectionPanel extends JPanel{
	
	private static String title;
	private ContentPanel content;
	private JPanel bPanel;
	
	private BufferedImage image;
	
	public SectionPanel(String s) throws IOException{
		System.out.println("-----------new Section Panel-----------" + s);
		
		title = s;
		
		content = new ContentPanel();
		content.setLayout(new GridLayout(5, 1));
		setLayout(new BorderLayout());
		
		setBorder(BorderFactory.createBevelBorder(1));
		
		
		setBackground(new Color(0, 0, 0, 75));
		
		
		
		JLabel title = new JLabel(s);
		title.setHorizontalAlignment(JLabel.CENTER);
		title.setVerticalAlignment(JLabel.TOP);
		JPanel titlePanel = new JPanel();
		titlePanel.setLayout(new BorderLayout());
		titlePanel.add(title, BorderLayout.CENTER);
		titlePanel.setBorder(BorderFactory.createLoweredBevelBorder());
		
		buildBPanel();
		
		
		add(titlePanel, BorderLayout.NORTH);
		add(bPanel, BorderLayout.SOUTH);
		add(content, BorderLayout.CENTER);
		setVisible(true);
	}
	
	private void buildBPanel(){
		bPanel = new JPanel();
		bPanel.setBorder(BorderFactory.createLoweredBevelBorder());
		
		if (title.equalsIgnoreCase("Done")) {
			JButton moveLeft = new JButton("<<");
			moveLeft.setHorizontalAlignment(JButton.RIGHT);
			moveLeft.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					try {
						BootWindow.doMove(-1);
					} catch (ClassNotFoundException | IOException e1) {
							
						e1.printStackTrace();
					}
				}
				
			});
			
			JButton clearAll = new JButton("Clear All");
			clearAll.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						new ConfirmDialog();
						
					} catch (IOException e1) {
						e1.printStackTrace();
						
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			});
			
			bPanel.setLayout(new BorderLayout());
			bPanel.add(moveLeft, BorderLayout.WEST);
			bPanel.add(clearAll, BorderLayout.EAST);
			
		}
		
		if (title.equalsIgnoreCase("Doing")){
			
			JButton moveRight = new JButton(">>");
			bPanel.setLayout(new BorderLayout());
			bPanel.add(moveRight, BorderLayout.EAST);
			moveRight.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					
					try {
						BootWindow.doMove(1);
					} catch (ClassNotFoundException | IOException e1) {
						
						e1.printStackTrace();
					}
					
				}
				
			});
			
			JButton moveLeft = new JButton("<<");
			moveLeft.addActionListener(new ActionListener() {

				
				public void actionPerformed(ActionEvent e) {
					
					try {
						BootWindow.doMove(-1);
					} catch (ClassNotFoundException | IOException e1) {

						e1.printStackTrace();
					}
				
				}
				
			});
			
			bPanel.add(moveLeft, BorderLayout.WEST);
			
		} 
		
		if (title.equalsIgnoreCase("To-Do")){
			
			JButton moveRight = new JButton(">>");
			moveRight.setHorizontalAlignment(JButton.RIGHT);
			bPanel.setLayout(new BorderLayout());
			
			moveRight.addActionListener(new ActionListener() {

				
				public void actionPerformed(ActionEvent e) {
					try {
						BootWindow.doMove(1);
					} catch (ClassNotFoundException e1) {
						
						e1.printStackTrace();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					}
					
				}
				
			});
			
			bPanel.add(moveRight, BorderLayout.EAST);
			
			JButton newTask = new JButton("Add Task");
			newTask.setHorizontalAlignment(JButton.LEFT);
			newTask.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						new NewTaskWindow();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					BootWindow.close();
					
				}
			});
			
			bPanel.add(newTask, BorderLayout.WEST);
		}
	}
	
	
	public void addToContentPanel(TaskPanel taskPanel) {
		content.add(taskPanel);
	}
	
	public String getTitle() {
		return title;
	}
	
}
