package portfolio;

import java.awt.*;
import java.io.*;
import java.util.*;

import javax.swing.JOptionPane;


public class DailyToDo{
	
	private static Scanner s;
	private static File taskFile;
	
	public static String[] tasks;
	private static String[] newTasks;
	private static ArrayList<String> taskArray = new ArrayList<String>();
	
	private static int toDoINT, doingINT, doneINT;
	
	
	public static void main(String[] args) throws IOException, ClassNotFoundException{
		
		taskFile = new File("./tasks.txt");
		
        if (taskFile.isFile()){
            System.out.println("tasks.txt Found");
            getTasks();

            newTasks = tasks;

        } else {
            if (taskFile.createNewFile()){
                System.out.println("Tasks File Created...");
            }
        }

        new BootWindow(tasks);
	}
	
	public static int getLines() throws IOException {
		//System.out.println("get lines");
		Scanner s1 = new Scanner(taskFile);
		int lines = 0;
		String ln;
		
		while (s1.hasNext()) {
			ln = s1.nextLine();
			
			if (ln != null) {
				lines++;
			}
		}
		
		System.out.println(lines + "<- Lines");
		s1.close();
		return lines;
	}
	
	public static String[] getTasks() throws IOException, ClassNotFoundException{
		int i = 0;
		taskFile = new File("./tasks.txt");
		
		taskArray = new ArrayList<String>();
		
        tasks = new String[getLines()];
        System.out.println();
        System.out.println("tasks size: " + tasks.length);
		s = new Scanner(taskFile);
		
		while(s.hasNext()){

			tasks[i] = s.nextLine();
			taskArray.add(tasks[i]);
	    	i++;
	    }
		
	    s.close();

	    System.out.println("\nGOT Tasks\n");

	    return tasks;
	}
	
	public static void changeFile(String taskName, String newSection) throws IOException, ClassNotFoundException{ 
		
		s = new Scanner(taskFile);
		int j = 0;
		
		newTasks = new String[getLines()];
		
		while(s.hasNext()) {
			newTasks[j] = s.nextLine();
			j++;
			
		}
		
		for (int i = 0; i < newTasks.length; i++) {
			//System.out.println(oldTasks[i].toString().substring(0, oldTasks[i].toString().indexOf("-")));
			
			if (newTasks[i].toString().substring(newTasks[i].toString().indexOf("'") + 1, newTasks[i].toString().lastIndexOf("'")).equalsIgnoreCase(taskName)) {
				//System.out.println("" + newSection + "-'" + taskName + "'");
				
				newTasks[i] = "" + newSection + "-'" + taskName + "'";
			}
		}
		s.close();
		
        PrintWriter pr = new PrintWriter(taskFile);

        System.out.println("\nSAVED TASKS\n");

        for (int i = 0; i < newTasks.length; i++){
            pr.println(newTasks[i]);
        }
        pr.close();
		
	}
	
	public static void checkAddTask(String s) throws ClassNotFoundException, IOException {
		Scanner s1 = new Scanner(taskFile);
		String ln;
		
		toDoINT = 0;
		doingINT = 0;
		doneINT = 0;
		
		while (s1.hasNext()) {
			ln = s1.nextLine();
			
			if (ln != null) {
				
				if (ln.startsWith("ToDo")) toDoINT++;
				if (ln.startsWith("Doing")) doingINT++;
				if (ln.startsWith("Done")) doneINT++;
			}
		}
		
		s1.close();
		
		System.out.println("ToDo Lines: " + toDoINT);
		System.out.println("Doing Lines: " + doingINT);
		System.out.println("Done Lines: " + doneINT);
		
		if (s.startsWith("ToDo")) {
			if (toDoINT >= 5) {
				JOptionPane.showMessageDialog(null, "To Many ToDo Tasks");
				
				new BootWindow(tasks);
			} else {
				addToFile(s);
			}
		}
		
		if (s.startsWith("Doing")) {
			if (doingINT >= 5) {
				JOptionPane.showMessageDialog(null, "To Many Doing Tasks");
				
				new BootWindow(tasks);
			} else {
				addToFile(s);
			}
		}
		
		if (s.startsWith("Done")) {
			if (doneINT >= 5) {
				JOptionPane.showMessageDialog(null, "To Many Done Tasks");
				
				new BootWindow(tasks);
			} else {
				addToFile(s);
			}
		}
		

	}
	
	public static void addToFile(String s) throws ClassNotFoundException, IOException {
		System.out.println("Task array holds the old tasks and is used to add a new task");
		
		System.out.println(s + "<- added to file");
		
		taskArray.add(s);
		
		
		//sets tasks to the updated arraylist (the old tasks plus 1 for the new one) {if i used arrayLists for the tasks array this could be simpler but i tried it and ran into issues and think this is easier.}
		
		tasks = new String[taskArray.size()];
		
		for (int i = 0; i < tasks.length; i++) {
			tasks[i] = taskArray.get(i);
		}
		
		PrintWriter pr = new PrintWriter(taskFile);
		
		for (int i = 0; i < taskArray.size(); i++) {
			pr.println(tasks[i]);
			
			//Updated file output.
			System.out.println("Updated file output: " + taskArray.get(i));
		}
		pr.close();
		
		new BootWindow(tasks);
		
		for (int i = 0; i < tasks.length; i++) {
			System.out.println(tasks[i]);
		}
			
		
	}
	
	public static void clearFile() throws IOException, ClassNotFoundException {
		if (taskFile.exists() && taskFile.isFile()) {
			taskFile.delete();
		}
		taskFile.createNewFile();
		
		new BootWindow(getTasks());
	}
	
}
