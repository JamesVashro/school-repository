package portfolio;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.Border;

public class BootWindow extends JFrame{
	
	private static JFrame window;
	
	private static final int WIDTH = 1000;
	private static final int HEIGHT = 720;
	
	private static String sToDo, sDoing, sDone;
	
	private static TaskPanel selectedPanel;
	
	private static JPanel mainPanel;
	
	private static ArrayList<TaskPanel> toDoPanels = new ArrayList<TaskPanel>();
	private static ArrayList<TaskPanel> doingPanels = new ArrayList<TaskPanel>();
	private static ArrayList<TaskPanel> donePanels = new ArrayList<TaskPanel>();
	
	private static SectionPanel toDo, doing, done;
	
	private static String[] tasks;
	
	
	public BootWindow(String[] t) throws IOException, ClassNotFoundException{
		
		
		toDoPanels.removeAll(toDoPanels);
		doingPanels.removeAll(doingPanels);
		donePanels.removeAll(donePanels);
		
		window = new JFrame();
		
		window.setSize(WIDTH, HEIGHT);
		window.setDefaultCloseOperation(EXIT_ON_CLOSE);
		window.setTitle("Daily To-Do");
		window.setLocationRelativeTo(null);
		window.setResizable(false);
		
		tasks = DailyToDo.getTasks();
		
		sToDo = new String("To-Do");
		sDoing = new String("Doing");
		sDone = new String("Done");
		
		
		for (int i = 0; i < tasks.length; i++) {
			System.out.println("new BootWindow Task: " + tasks[i]);
		}
		
		buildMainPanel();
	}
	
	private static void buildMainPanel() throws ClassNotFoundException, IOException {
		mainPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(1, 3));
		
		//mainPanel.setBackground(Color.blue); //For debugging reasons
		
		buildToDo();
	}
	
	private static void buildToDo() throws ClassNotFoundException, IOException {
		toDo = new SectionPanel(sToDo);
		int TODOINT = 0;
		
		ArrayList<String> strings = new ArrayList<String>();
		
		
		for (int i = 0; i < tasks.length; i++) {
			if (tasks[i].startsWith("ToDo-")) {
				TODOINT++;
				
				strings.add(tasks[i].toString().substring(tasks[i].toString().indexOf("'") + 1, tasks[i].toString().lastIndexOf("'")));
				
			}
		}
		
		for (int i = 0; i < TODOINT; i++) {
			
			toDoPanels.add(new TaskPanel(strings.get(i), i, toDo.getTitle()));
			
			final int onPanel = i;
			
			toDoPanels.get(i).addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					selectPanel(toDoPanels, onPanel);
					toggleSelection(toDoPanels.get(onPanel), toDoPanels);
				}

				@Override
				public void mousePressed(MouseEvent e) {
					//toDoPanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLineBorder(Color.WHITE, 2)));
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					//toDoPanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.WHITE, 1), BorderFactory.createRaisedBevelBorder()));
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					toDoPanels.get(onPanel).setBackground(new Color(120, 121, 122));
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					toDoPanels.get(onPanel).setBackground(new Color(201, 201, 201));
					
					
				}
				
			});
			
			
			toDo.addToContentPanel(toDoPanels.get(i));
		}
		
		mainPanel.add(toDo);
		
		buildDoing();
	}
	
	
	private static void buildDoing() throws ClassNotFoundException, IOException {
		doing = new SectionPanel(sDoing);
		int DOINGINT = 0;
		ArrayList<String> strings = new ArrayList<String>();

		for (int i = 0; i < tasks.length; i++) {
			if (tasks[i].startsWith("Doing-")) {
				DOINGINT++;
				strings.add(tasks[i].toString().substring(tasks[i].toString().indexOf("'") + 1, tasks[i].toString().lastIndexOf("'")));
			}
		}
		
		for (int i = 0; i < DOINGINT; i++) {
			doingPanels.add(new TaskPanel(strings.get(i), i, doing.getTitle()));
			
			final int onPanel = i;
			
			doingPanels.get(i).addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					selectPanel(doingPanels, onPanel);
					toggleSelection(doingPanels.get(onPanel), doingPanels);
				}

				@Override
				public void mousePressed(MouseEvent e) {
					//doingPanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLineBorder(Color.WHITE, 2)));
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					//doingPanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.WHITE, 1), BorderFactory.createRaisedBevelBorder()));
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					doingPanels.get(onPanel).setBackground(new Color(120, 121, 122));
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					doingPanels.get(onPanel).setBackground(new Color(201, 201, 201));
					
				}
				
			});
			
			
			doing.addToContentPanel(doingPanels.get(i));
		}
		
		
		mainPanel.add(doing);
		
		
		buildDone();
	}
	
	private static void buildDone() throws ClassNotFoundException, IOException {
		done = new SectionPanel(sDone);
		int DONEINT = 0;
		ArrayList<String> strings = new ArrayList<String>();
		
		for (int i = 0; i < tasks.length; i++) {
			if (tasks[i].startsWith("Done-")) {
				DONEINT++;
				
				strings.add(tasks[i].toString().substring(tasks[i].toString().indexOf("'") + 1, tasks[i].toString().lastIndexOf("'")));
			}
		}
		
		for (int i = 0; i < DONEINT; i++) {
			
			donePanels.add(new TaskPanel(strings.get(i), i, done.getTitle()));
			
			final int onPanel = i;
			
			donePanels.get(i).addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					selectPanel(donePanels, onPanel);
					toggleSelection(donePanels.get(onPanel), donePanels);
				}

				@Override
				public void mousePressed(MouseEvent e) {
					//donePanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLineBorder(Color.WHITE, 2)));
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					//donePanels.get(onPanel).setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.WHITE, 1), BorderFactory.createRaisedBevelBorder()));
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					donePanels.get(onPanel).setBackground(new Color(120, 121, 122));
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					donePanels.get(onPanel).setBackground(new Color(201, 201, 201));
					
				}
				
			});
			
			
			done.addToContentPanel(donePanels.get(i));
		}
		
		
		mainPanel.add(done);
		window.add(mainPanel);
		window.setVisible(true);
		
		
	}
	
	private static void toggleSelection(TaskPanel panel, ArrayList<TaskPanel> panels) {
		
		Border selectedBorder = BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLineBorder(Color.BLACK, 3));
		Border normalBorder = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.WHITE, 1), BorderFactory.createRaisedBevelBorder());
		
		for (int i = 0; i < panels.size(); i++) {
			if (panels.get(i).getID() == panel.getID()) {
				
				panel.setBorder(selectedBorder);
			} else {
				
				panels.get(i).setBorder(normalBorder);
			}
		}
		
		if (panels.equals(toDoPanels)) {
			System.out.println("selected Panel Array: TODO --------------------");
			
			for (int i = 0; i < doingPanels.size(); i++) {
				
				doingPanels.get(i).setBorder(normalBorder);
				
			}
			
			for (int i = 0; i < donePanels.size(); i++) {
				donePanels.get(i).setBorder(normalBorder);
				
			}
		}
		
		if (panels.equals(doingPanels)) {
			
			for (int i = 0; i < toDoPanels.size(); i++) {
				toDoPanels.get(i).setBorder(normalBorder);
			}
			
			for (int i = 0; i < donePanels.size(); i++) {
				
				donePanels.get(i).setBorder(normalBorder);
				
			}
		}
		
		if (panels.equals(donePanels)) {
			for (int i = 0; i < toDoPanels.size(); i++) {
				toDoPanels.get(i).setBorder(normalBorder);
			}
			
			for (int i = 0; i < doingPanels.size(); i++) {
				doingPanels.get(i).setBorder(normalBorder);
			}
		}
		
	}
	
	public static void close() {
		window.dispose();
	}
	
	
	public static void selectPanel(ArrayList<TaskPanel> parent, int panel) {
		//System.out.println("parent = " + parent.get(panel).getParentPanelStr());
		
		//System.out.println("task: " + parent.get(panel).getTask());
		
		
		selectedPanel = parent.get(panel);
	}
	
	public static TaskPanel getSelectedPanel() {
		return selectedPanel;
	}
	
	//int dir is the direction: -1 = left, 1 = right;
	public static void doMove(int dir) throws ClassNotFoundException, IOException {
		int value = 0;
		ArrayList<TaskPanel> panelsToUse = new ArrayList<TaskPanel>();
		ArrayList<TaskPanel> panelMoveTo = new ArrayList<TaskPanel>();
		
		if (selectedPanel.getParentPanelStr().equalsIgnoreCase("To-Do")) {
			value = 1;
			panelsToUse = toDoPanels;
			panelMoveTo = doingPanels;
		}
		
		if (selectedPanel.getParentPanelStr().equalsIgnoreCase("Doing")) {
			value = 2;
			panelsToUse = doingPanels;
			panelMoveTo = donePanels; 
			
		}
		
		if (selectedPanel.getParentPanelStr().equalsIgnoreCase("Done")) {
			value = 3;
			panelsToUse = donePanels;
			panelMoveTo = doingPanels;
		}
		
		for (int i = 0; i < panelsToUse.size(); i++) {
			if (i > selectedPanel.getID()) {
				System.out.println(selectedPanel.getID() + ": selectedPanels ID");
				System.out.println(panelsToUse.get(i).getID() + "--Panel Above selected Panel ID");
				
				panelsToUse.get(i).changeID(panelsToUse.get(i).getID() - 1);
				
				System.out.println(panelsToUse.get(i).getID() + "--new Panel IDS");
			}
		}
		
		panelsToUse.remove(selectedPanel);
		panelMoveTo.add(selectedPanel);
		
		
		//System.out.println(Section.valueOf(value + dir).toString());
		
		DailyToDo.changeFile(selectedPanel.getTask(), Section.valueOf(value + dir).toString());
		
		toDoPanels.removeAll(toDoPanels);
		doingPanels.removeAll(doingPanels);
		donePanels.removeAll(donePanels);
		
		
		window.dispose();
		new BootWindow(DailyToDo.getTasks());
		
	}
	
	public static void clearAll() throws IOException, ClassNotFoundException {
		toDoPanels.removeAll(toDoPanels);
		doingPanels.removeAll(doingPanels);
		donePanels.removeAll(donePanels);
		
		window.dispose();
		
		DailyToDo.clearFile();
		
		
	}
	
}
