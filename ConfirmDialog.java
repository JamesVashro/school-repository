package portfolio;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;

public class ConfirmDialog extends JFrame{
	private int width = 300;
	private int height = 450;
	
	private JPanel mainPanel;
	private JPanel bPanel;
	private JPanel tPanel;
	private JPanel cPanel;
	
	private boolean fileEmpty;
	
	private int input;
	
	public ConfirmDialog() throws IOException, ClassNotFoundException {
		super();
		
		setSize(width, height);
		setTitle("Clear File");
		setResizable(false);
		setLocationRelativeTo(null);
		
		mainPanel = new ContentPanel();
		
		mainPanel.setLayout(new BorderLayout());
		
		buildTPanel();
		
		add(mainPanel);
		
		setVisible(true);
	}
	
	private void buildTPanel() throws IOException, ClassNotFoundException {
		tPanel = new ContentPanel();
		tPanel.setLayout(new FlowLayout());
		
		JLabel title = new JLabel("Are you sure you want to clear file?");
		title.setHorizontalAlignment(JLabel.CENTER);
		title.setForeground(Color.white);
		
		tPanel.add(title);
		
		mainPanel.add(tPanel, BorderLayout.NORTH);
		buildCPanel();
		
	}
	
	private void buildCPanel() throws IOException, ClassNotFoundException {
		cPanel = new ContentPanel();
		JTextArea textArea = new JTextArea(20, 20);
		textArea.setEditable(false);
		textArea.setOpaque(false);

		String[] tasks = DailyToDo.getTasks();
		
		textArea.setForeground(Color.WHITE);
		
		if (tasks.length <= 0) {
			fileEmpty = true;
			textArea.append("File is empty");
		} else {
			for (int i = 0; i < tasks.length; i++) {
				textArea.append(tasks[i] + "\n\n");
			}
		}
		
		
		
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.getViewport().setOpaque(false);
		scrollPane.setOpaque(false);
		
		scrollPane.setBorder(null);
		
		
		
		cPanel.add(scrollPane);
		
		mainPanel.add(cPanel, BorderLayout.CENTER);
		buildBPanel();
	}
	
	private void buildBPanel() throws IOException {
		bPanel = new ContentPanel();
		bPanel.setLayout(new FlowLayout());
		
		JButton yes = new JButton("Im Sure");
		JButton no = new JButton("Cancel");
		
		if (!fileEmpty) yes.addActionListener(new ClearListener());
		else yes.addActionListener(new CloseListener());
		no.addActionListener(new CloseListener());
		
		bPanel.add(yes);
		bPanel.add(no);
		
		mainPanel.add(bPanel, BorderLayout.SOUTH);
	}
	
	private class ClearListener implements ActionListener {

		
		public void actionPerformed(ActionEvent e) {
			try {
				dispose();
				BootWindow.clearAll();
			} catch (ClassNotFoundException | IOException e1) {
				
				e1.printStackTrace();
			}
		}
	}
	
	private class CloseListener implements ActionListener{	
		public void actionPerformed(ActionEvent e) {
			dispose();
			
		}
	}

}


