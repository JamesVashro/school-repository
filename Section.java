package portfolio;

import java.util.HashMap;
import java.util.Map;

public enum Section {
	ToDo(1),
	Doing(2),
	Done(3);
	
	private int value;
	private static Map<Object, Object> map = new HashMap<>();
	
	Section(int value) {
		this.value = value;
	}
	
	static {
		for (Section s : Section.values()) {
			map.put(s.value, s);
		}
	}
	
	public int getValue() {
		return this.value;
	}
	
	public static Section valueOf(int val) {
		return (Section) map.get(val);
	}
}
