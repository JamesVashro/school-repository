package portfolio;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import javax.swing.*;

public class NewTaskWindow extends JFrame{
	
	private JPanel titlePanel, mainPanel, bPanel;
	private String newLine;
	private JTextArea input;
	private JCheckBox todo, doing, done;
	
	
	public NewTaskWindow() throws IOException {
		setTitle("New Task");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setSize(303, 450);
		
		setLayout(new BorderLayout());
		
		setLocationRelativeTo(null);
		
		setVisible(true);
		
		buildTitlePanel();
		buildMainPanel();
		buildButtonPanel();
	}
	
	private void buildButtonPanel() throws IOException {
		bPanel = new ContentPanel();
		bPanel.setLayout(new BorderLayout());
		
		JButton close = new JButton("Cancel");
		JButton addTask = new JButton("Add Task");
		
		bPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		close.addActionListener(new CloseListener());
		addTask.addActionListener(new AddTaskListener());
		
		bPanel.add(addTask, BorderLayout.EAST);
		bPanel.add(close, BorderLayout.WEST);
		add(bPanel, BorderLayout.SOUTH);
	}

	private void buildMainPanel() throws IOException {
		mainPanel = new ContentPanel();
		mainPanel.setLayout(new GridLayout(2, 1));
		
		mainPanel.setBackground(Color.BLACK);
		
		mainPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		
		input = new JTextArea();
		input.setBorder(BorderFactory.createLineBorder(Color.black));
		
		JPanel checkPanel = new JPanel();
		checkPanel.setOpaque(false);
		
		checkPanel.setLayout(new FlowLayout());
		todo = new JCheckBox("ToDo");
		doing = new JCheckBox("Doing");
		done = new JCheckBox("Done");
		
		todo.setForeground(Color.WHITE);
		doing.setForeground(Color.WHITE);
		done.setForeground(Color.WHITE);
		
		todo.setOpaque(false);
		doing.setOpaque(false);
		done.setOpaque(false);
		
		ButtonGroup group = new ButtonGroup();
		
		group.add(todo);
		group.add(doing);
		group.add(done);
		
		checkPanel.add(todo);
		checkPanel.add(doing);
		checkPanel.add(done);
		
		mainPanel.add(input);
		mainPanel.add(checkPanel);
		add(mainPanel);
	}

	private void buildTitlePanel() throws IOException {
		titlePanel = new ContentPanel();
		titlePanel.setLayout(new FlowLayout());
		
		JLabel title = new JLabel("New Task");
		title.setForeground(Color.WHITE);
		
		title.setHorizontalAlignment(JLabel.CENTER);
		
		titlePanel.add(title);
		add(titlePanel, BorderLayout.NORTH);
	}
	
	private class AddTaskListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			newLine = input.getText();
			String toFile;
			
			if (todo.isSelected()) {
				//System.out.println(todo.getText() + "-'" + newLine + "'");
				toFile = todo.getText() + "-'" + newLine + "'";
				
				
				try {
					DailyToDo.checkAddTask(toFile);
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				dispose();
				
			}
			
			if (doing.isSelected()) {
				//System.out.println(doing.getText() + "-'" + newLine + "'");
				toFile = doing.getText() + "-'" + newLine + "'";
				
				
				try {
					DailyToDo.checkAddTask(toFile);
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				dispose();
					
			}
			
			if (done.isSelected()) {
				//System.out.println(done.getText() + "-'" + newLine + "'");
				toFile = done.getText() + "-'" + newLine + "'";
				
				
				try {
					DailyToDo.checkAddTask(toFile);
				} catch (ClassNotFoundException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				dispose();
			}
			
			
		}
	}
	
	private class CloseListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			dispose();
			
			try {
				new BootWindow(DailyToDo.getTasks());
				
			} catch (ClassNotFoundException | IOException e1) {}
			
		}
		
	}
}
